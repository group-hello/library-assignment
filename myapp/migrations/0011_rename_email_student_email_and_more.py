# Generated by Django 4.0.5 on 2022-09-01 18:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0010_merge_20220819_0955'),
    ]

    operations = [
        migrations.RenameField(
            model_name='student',
            old_name='Email',
            new_name='email',
        ),
        migrations.RenameField(
            model_name='student',
            old_name='FirstName',
            new_name='first_name',
        ),
        migrations.RenameField(
            model_name='student',
            old_name='LastName',
            new_name='last_name',
        ),
        migrations.RenameField(
            model_name='student',
            old_name='Password',
            new_name='password1',
        ),
        migrations.RenameField(
            model_name='student',
            old_name='UserName',
            new_name='username',
        ),
        migrations.AddField(
            model_name='student',
            name='password2',
            field=models.CharField(default=True, max_length=128),
        ),
    ]
